from django.urls import path
from my_app import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('projects/', views.projects, name='projects'),
    path('hobby/', views.hobbies, name='hobby'),
    path('contact/', views.contact, name='contact')
]